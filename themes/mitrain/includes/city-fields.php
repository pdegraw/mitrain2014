<?php if( get_field('greeting_content') ): ?>
	<div class="clear"></div>
	<hr/>
	<h2>Arriving in <?php the_title(); ?></h2>
	<strong>Just got into <?php the_title(); ?>?</strong>
	<?php if( get_field('greeting_content') ): ?>
		<?php the_field('greeting_content'); ?>
	<?php endif; ?>
	<div class="column twocol">
		<?php if( get_field('taxi_content') ): ?>
			<div class="accordionButton"><img class="alignleft arrivalbutton-taxi" src="/wp-content/uploads/2014/02/taxi.jpg" alt="taxi" width="202" height="48" /></div>
				<div class="accordionContent">
						<?php the_field('taxi_content'); ?>
				</div>
		<?php endif; ?>
		<?php if( get_field('bus_content') ): ?>
			<div class="accordionButton"><img class="alignleft arrivalbutton" src="/wp-content/uploads/2014/02/bus.jpg" alt="bus" width="198" height="48" /></div>
				<div class="accordionContent">
						<?php the_field('bus_content'); ?>
				</div>
		<?php endif; ?>
		<?php if( get_field('car_rental_content') ): ?>
		<div class="accordionButton"><img class="alignleft arrivalbutton" src="/wp-content/uploads/2014/02/rental.jpg" alt="rental" width="200" height="48" /></div>
			<div class="accordionContent">
					<?php the_field('car_rental_content'); ?>
			</div>
		<?php endif; ?>
	</div>
	</div>
	<div class="column twocol">
			<?php if( get_field('map_url') ): ?>
				<?php the_field('map_url'); ?>
			<?php endif; ?>
		<div class="clear"></div>
	</div>
<?php endif; ?>