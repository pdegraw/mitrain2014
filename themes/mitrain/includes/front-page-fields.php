<?php 
		 
// get an image field (return type = object)
$image1 = get_field('image_1');
$image2 = get_field('image_2');
$image3 = get_field('image_3');

?>

<div class="entries" style="position: relative;">

<article class="column threecol">
<div class="entry">
	<figure class="entry-thumbnail">
		<a href="<?php the_field('link_1'); ?>">
			<img src="<?php echo $image1['url']; ?>" alt="<?php echo $image1['alt']; ?>" />
		</a>
	</figure>
	<div class="entry-container">
		<?php if( get_field('title_1') ): ?>
			<header class="entry-header">
				<h2 class="entry-title">
					<a title="Pere Marquette" href="<?php the_field('link_1'); ?>" rel="bookmark">
						<?php the_field('title_1'); ?>
					</a></h2>
			</header>
		<?php endif; ?>
		<?php if( get_field('content_1') ): ?>
			<div class="entry-summary">
				<p><?php the_field('content_1'); ?></p>
			</div>
		<?php endif; ?>
	<div class="clear"></div>
	</div>
</div>
</article>

<article class="column threecol">
<div class="entry">
	<figure class="entry-thumbnail">
		<a href="<?php the_field('link_2'); ?>">
			<img src="<?php echo $image2['url']; ?>" alt="<?php echo $image2['alt']; ?>" />
		</a>
	</figure>
	<div class="entry-container">
		<?php if( get_field('title_2') ): ?>
			<header class="entry-header">
				<h2 class="entry-title">
					<a title="Pere Marquette" href="<?php the_field('link_2'); ?>" rel="bookmark">
						<?php the_field('title_2'); ?>
					</a></h2>
			</header>
		<?php endif; ?>
		<?php if( get_field('content_2') ): ?>
			<div class="entry-summary">
				<p><?php the_field('content_2'); ?></p>
			</div>
		<?php endif; ?>
	<div class="clear"></div>
	</div>
</div>
</article>


<article class="column threecol">
<div class="entry">
	<figure class="entry-thumbnail">
		<a href="<?php the_field('link_3'); ?>">
			<img src="<?php echo $image3['url']; ?>" alt="<?php echo $image3['alt']; ?>" />
		</a>
	</figure>
	<div class="entry-container">
		<?php if( get_field('title_3') ): ?>
			<header class="entry-header">
				<h2 class="entry-title">
					<a title="Pere Marquette" href="<?php the_field('link_3'); ?>" rel="bookmark">
						<?php the_field('title_3'); ?>
					</a></h2>
			</header>
		<?php endif; ?>
		<?php if( get_field('content_3') ): ?>
			<div class="entry-summary">
				<p><?php the_field('content_3'); ?></p>
			</div>
		<?php endif; ?>
	<div class="clear"></div>
	</div>
</div>
</article>
</div>