<?php if ( is_active_sidebar( 'train-lines-sidebar' ) or is_page( array( 2, 13, 15 ) ) ) : ?>
	<div class="column fourcol" role="complementary">
		<div id="sidebar-right" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'train-lines-sidebar' ); ?>
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
<?php endif; ?>
<?php if ( is_active_sidebar( 'pm-sidebar') && is_page(2) || '2' == $post->post_parent ) : ?>
	<div class="column fourcol" role="complementary">
		<div id="sidebar-right" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'pm-sidebar' ); ?>
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
<?php elseif ( is_active_sidebar( 'bw-sidebar') && is_page(15) ) : ?>
	<div class="column fourcol" role="complementary">
		<div id="sidebar-right" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'bw-sidebar' ); ?>
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
<?php elseif ( is_active_sidebar( 'w-sidebar') && is_page(13) ) : ?>
	<div class="column fourcol" role="complementary">
		<div id="sidebar-right" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'w-sidebar' ); ?>
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
<?php elseif ( is_active_sidebar( 'blog-sidebar') && 'post' == get_post_type() ) : ?>
	<div class="column fourcol" role="complementary">
		<div id="sidebar-right" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'blog-sidebar' ); ?>
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
<?php elseif ( is_active_sidebar( 'basic-page-sidebar') ) : ?>
	<div class="column fourcol" role="complementary">
		<div id="sidebar-right" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'basic-page-sidebar' ); ?>
		</div><!-- .widget-area -->
	</div><!-- #secondary -->
<?php endif; ?>