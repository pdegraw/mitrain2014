<?php
/*
Template Name: Front Page
*/
?><?php get_header(); ?>

<div style="padding-top: 56.2121%;" class="fluid-width-video-wrapper">
	<iframe id="fitvid361947" src="http://www.youtube.com/embed/gWKLWjENS7c?feature=oembed" allowfullscreen="" frameborder="0"></iframe>
</div>
	<div id="container">
		<section id="content" class="column onecol">
			<?php if( have_posts() ) : the_post(); ?>
				<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">
					<div class="entry">
						<header class="entry-header">
							<<?php pinboard_title_tag( 'post' ); ?> class="entry-title"><?php the_title(); ?></<?php pinboard_title_tag( 'post' ); ?>>
						</header><!-- .entry-header -->
						<div class="entry-content">
							<?php the_content(); ?>
							
							<?php include ('includes/front-page-fields.php'); ?>
							<div class="clear"></div>
						</div><!-- .entry-content -->
						<?php wp_link_pages( array( 'before' => '<footer class="entry-utility"><p class="post-pagination">' . __( 'Pages:', 'pinboard' ), 'after' => '</p></footer><!-- .entry-utility -->' ) ); ?>
					</div><!-- .entry -->
				</article><!-- .post -->
			<?php else : ?>
				<?php pinboard_404(); ?>
			<?php endif; ?>
		</section><!-- #content -->
		<div class="clear"></div>
	</div><!-- #container -->
	<?php if( pinboard_get_option( 'slider' ) ) : ?>
		<?php get_template_part( 'slider' ); ?>
	<?php endif; ?>
	<?php get_sidebar( 'wide' ); ?>
	<?php get_sidebar( 'boxes' ); ?>
<?php get_footer(); ?>